#!/usr/bin/php
<?php

$deploy_hook_filename 	= '/opt/elasticbeanstalk/hooks/appdeploy/enact/02_s3fs.sh';
$deploy_hook_contents 	= '';
$current_app_directory 	= '/var/app/current/jade-video';
$s3fs_mounts 			= array();

$folder = "videocontent";


$deploy_hook_contents =  "#!/usr/bin/env bash\n";
//$deploy_hook_contents .= ". /opt/elasticbeanstalk/support/envvars\n";

echo "App directory: $current_app_directory\n";

// work from inside the app directory
chdir($current_app_directory);

// does the local folder exist?
// if not, create it.
if (!file_exists($folder)) {
    echo "Making the local folder: $current_app_directory/{$folder}\n";
    mkdir($folder, 0775, TRUE);
    shell_exec("chown webapp.webapp {$folder}");
}

/**
 * Don't create the mounts, put a hook script into elastic beanstalk's deploy process
 */
$mount_command = "sudo s3fs jadepreview -o endpoint=eu-west-3 -o use_cache=/tmp -o allow_other -o uid=996 -o gid=994 -o mp_umask=007 " . $folder; // /var/app/current/jade-video/videocontent
$deploy_hook_contents .= $mount_command."\n";


// write the contents of the file
if (!empty($deploy_hook_contents)) {
    file_put_contents($deploy_hook_filename, $deploy_hook_contents);
    chmod($deploy_hook_filename, 0744);
}
?>