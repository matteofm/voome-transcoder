<?php
    header("Access-Control-Allow-Origin: *");

    require 'vendor/autoload.php';

    use Aws\S3\S3Client;
    use Aws\S3\Exception\S3Exception;
    use Imagick;

    $thumbSettings = array(
        /*'xs_square' => array(
            'width' => '45',
            'height' => '45',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '70',
            'suffix' => '',
            'back' => false),*/
        'xs' => array(
            'width' => '80',
            'height' => '45',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '70',
            'suffix' => '',
            'back' => false),
        /*'s_square' => array(
            'width' => '90',
            'height' => '90',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => 80,
            'suffix' => '',
            'back' => true),*/
        's' => array(
            'width' => '160',
            'height' => '90',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => 80,
            'suffix' => '',
            'back' => true),
        /*'m_square' => array(
            'width' => '135',
            'height' => '135',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '70',
            'suffix' => '',
            'back' => false),*/
        'm' => array(
            'width' => '240',
            'height' => '135',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '70',
            'suffix' => '',
            'back' => false),
        /*'l_square' => array(
            'width' => '270',
            'height' => '270',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '75',
            'suffix' => '',
            'back' => false),*/
        'l' => array(
            'width' => '480',
            'height' => '270',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '75',
            'suffix' => '',
            'back' => false),
        'xl' => array(
            'width' => '960',
            'height' => '540',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '80',
            'suffix' => '',
            'back' => false),
        'xxl' => array(
            'width' => '1280',
            'height' => '720',
            'format' => 'jpg',
            'bgr' => '#ffffff',
            'quality' => '85',
            'suffix' => '',
            'back' => false)
    );

    if (!file_exists("videocontent"))
        mkdir("videocontent", 0777, true);
    if (!file_exists("log"))
        mkdir("log", 0777, true);

    $ffmpeg_path = '/usr/bin/';

    $json = file_get_contents('php://input');
    $data = json_decode($json, true);
    $s3secret = $data["secret"];
    $s3key = $data["apikey"];
    $s3bucket = $data["bucket"];
    $srcKey = $data["key"];
    $type = $data["type"];

    function build_webp($srcKey, $srcName, $extension) {
        $destNamePreview = str_replace('.' . $extension, '.webp', $srcName);
        $destKeyPreview = str_replace('.' . $extension, '.webp', $srcKey);
        $destPreviewFile = __DIR__ . '/videocontent/' . $destKeyPreview;

        $ffmpegParams = " -lossless 0 -vf fps=3,scale=-1:480 -threads 0 ";
        $logFile = __DIR__ . '/log/transcoding_' . $destNamePreview . '.log';
        $srcFile = __DIR__ . '/videocontent/' . $srcKey;

        try {
            exec_ffmpeg($ffmpegParams, $srcFile, $destPreviewFile, $logFile);
            return array(
                "code" => 200,
                "key" => $destKeyPreview,
                "type" => "webp"
            );
        }
        catch (Exception $e) {
            return array(
                "code" => 500,
                "key" => "",
                "type" => "webp",
                "message" => $e->getMessage()
            );
        }
    }
    function build_webm($srcKey, $srcName, $extension) {
        $destNamePreview = str_replace('.' . $extension, '.webm', $srcName);
        $destKeyPreview = str_replace('.' . $extension, '.webm', $srcKey);
        $destPreviewFile = __DIR__ . '/videocontent/' . $destKeyPreview;

        //$command = $ffmpeg_path . "ffmpeg  -i " . $srcFile . " -b:v 0  -crf 30  -pass 1  -an -f webm /dev/null && ";
        //$command .= $ffmpeg_path . "ffmpeg  -i " . $srcFile . " -b:v 0  -crf 30  -pass 2 " . $destPreviewFile . " > " . $logFile . "&";

        $ffmpegParams = " -c:v libvpx -qmin 15 -qmax 25 -b:v 1M -threads 0 ";
        $logFile = __DIR__ . '/log/transcoding_' . $destNamePreview . '.log';
        $srcFile = __DIR__ . '/videocontent/' . $srcKey;

        try {
            exec_ffmpeg($ffmpegParams, $srcFile, $destPreviewFile, $logFile);
            return array(
                "code" => 200,
                "key" => $destKeyPreview,
                "type" => "webm"
            );
        }
        catch (Exception $e) {
            return array(
                "code" => 500,
                "key" => "",
                "type" => "webm",
                "message" => $e->getMessage()
            );
        }
    }
    function build_preview_webm($srcKey, $srcName, $extension) {
        $destNamePreview = str_replace('.' . $extension, '_prw.webm', $srcName);
        $destKeyPreview = str_replace('.' . $extension, '_prw.webm', $srcKey);
        $destPreviewFile = __DIR__ . '/videocontent/' . $destKeyPreview;

        $ffmpegParams = " -c:v libvpx -qmin 0 -qmax 25 -crf 4 -b:v 1M -vf scale=-1:720 -threads 0 ";
        $logFile = __DIR__ . '/log/transcoding_' . $destNamePreview . '.log';
        $srcFile = __DIR__ . '/videocontent/' . $srcKey;

        try {
            exec_ffmpeg($ffmpegParams, $srcFile, $destPreviewFile, $logFile);
            return array(
                "code" => 200,
                "key" => $destKeyPreview,
                "type" => "preview_webm"
            );
        }
        catch (Exception $e) {
            return array(
                "code" => 500,
                "key" => "",
                "type" => "preview_webm",
                "message" => $e->getMessage()
            );
        }
    }
    function exec_ffmpeg($ffmpegParams, $srcFile, $destPreviewFile, $logFile) {
        $ffmpeg_path = '/usr/bin/';
        exec($ffmpeg_path . "ffmpeg -i " .  $srcFile . $ffmpegParams . $destPreviewFile . " > " . $logFile);
    }

    $results = array();

    $srcKeyParts = explode('/', $srcKey);
    $srcFolder = "/";
    if (count($srcKeyParts) == 1)
        $srcName = $srcKeyParts[0];
    else {
        $srcName = end($srcKeyParts);
        unset($srcKeyParts[count($srcKeyParts) - 1]);
        $srcFolder = "/" . implode('/', $srcKeyParts);
    }

    $simpleName = explode('.', $srcName);
    $simpleName = explode('.', $srcName);
    $extension = end($simpleName);

    $s3Client = S3Client::factory(array(
        'version' => 'latest',
        'region' => 'eu-west-3',
        'credentials' => [
            'key'    => $s3key,
            'secret' => $s3secret
        ]
    ));

    $info = $s3Client->doesObjectExist($s3bucket, $srcKey);
    if ($info) {
        $srcFile = __DIR__ . '/videocontent/' . $srcKey;
        $srcFolder = __DIR__ . '/videocontent/' . $srcFolder;

        if (!file_exists($srcFolder))
            mkdir($srcFolder, 0777, true);

        $media = $s3Client->getObject(array(
            'Bucket' => $s3bucket,
            'Key'    => $srcKey,
            'SaveAs' => $srcFile
        ));

        switch ($type) {
            case "webm":
                $res = build_webm($srcKey, $srcName, $extension);
                break;
            case "preview_webm":
                $res = build_preview_webm($srcKey, $srcName, $extension);
                break;
            case "webp":
                $res = build_webp($srcKey, $srcName, $extension);
                break;
        }
        echo json_encode($res);

        if ($res["code"] == 200) {
            $destPreviewFile = __DIR__ . '/videocontent/' . $res["key"];
            $result = $s3Client->putObject(array(
                'Bucket'     => $s3bucket,
                'Key'        => $res["key"],
                'SourceFile' => $destPreviewFile
            ));
            $previewURL = $result["ObjectURL"];
            unlink($destPreviewFile);
        }
    }
    else {
        echo json_encode(array(
            "code" => 404,
            "message" => "File not found -> " . $srcFile . "!"
        ));
    }
